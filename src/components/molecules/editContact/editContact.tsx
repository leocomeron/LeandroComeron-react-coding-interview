import { useContactList } from '@hooks/contacts/useContactList';
import { IPerson } from '@lib/models/person';
import { TextField } from '@mui/material';
import { useParams } from 'react-router-dom';

export interface IEditContactProps {
  person?: IPerson;
}

const EditContact: React.FC<IEditContactProps> = ({ person }) => {
  const { id } = useParams();
  return (
    <div>
      <form>
        <div>
          <TextField id="first-name" label="First Name" defaultValue={''} />
        </div>
        <div>
          <TextField id="last-name" label="Last Name" />
        </div>
        <div>
          <TextField id="email" label="Email" />
        </div>
      </form>
    </div>
  );
};

export default EditContact;
